// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package argo

import (
	"net"
	"os"
)

type Listener struct {
	file  *os.File
	laddr *XenAddr
}

func ListenStream(addr string) (*Listener, error) {
	laddr, err := parseAddr(addr)
	if err != nil {
		return nil, err
	}

	file, err := os.Open("/dev/argo_stream")
	if err != nil {
		return nil, err
	}

	l := &Listener{
		file:  file,
		laddr: laddr,
	}

	// TODO: Allow specification of parter rather than
	//	 forcing DOMID_ANY.
	rid := ringID{
		domid:   uint16(0x7FF4), /* DOMID_ANY */
		partner: uint16(0x7FF4), /* DOMID_ANY */
		port:    laddr.Port,
	}

	_, err = ioctl(l.file.Fd(), iocBind, rid.bytes())
	if err != nil {
		l.Close()

		return nil, err
	}

	// Unused uint32 param for ARGOIOCLISTEN
	unused := make([]byte, 4)
	_, err = ioctl(l.file.Fd(), iocListen, unused)
	if err != nil {
		l.Close()

		return nil, err
	}

	return l, nil
}

func (l *Listener) Accept() (net.Conn, error) {
	var raddr XenAddr

	arg := raddr.bytes()

	fd, err := ioctl(l.file.Fd(), iocAccept, arg)
	if err != nil {
		return nil, err
	}

	raddr.decode(arg)

	file := os.NewFile(fd, l.file.Name())
	if err != nil {
		return nil, err
	}

	c := &Conn{
		file:  file,
		ptype: stream,
		laddr: l.laddr,
		raddr: &raddr,
	}

	return c, nil
}

func (l *Listener) Close() error {
	return l.file.Close()
}

func (l *Listener) Addr() net.Addr {
	return l.laddr
}
