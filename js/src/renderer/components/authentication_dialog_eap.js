import React, {Component} from 'react';

import Button from './button';
import InputField from './input_field';

// AuthenticationDialogEAP is the dialog used for 802.1x/WPA-EAP networks.
export default class AuthenticationDialogEAP extends Component {
    constructor(props) {
        super(props);

        this.state = {
            identity: undefined,
            password: undefined,
        };

        this.onChangeUserNameField = this.onChangeUserNameField.bind(this);
        this.onChangePasswordField = this.onChangePasswordField.bind(this);
        this.onClickNext = this.onClickNext.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    onChangeUserNameField(value) {
        this.setState((state) => ({...state, identity: value}));
    }

    onChangePasswordField(value) {
        this.setState((state) => ({...state, password: value}));
    }

    onClickNext(event) {
        event.stopPropagation();

        const auth = {
            identity: this.state.identity,
            password: this.state.password
        };

        this.props.submitAuthData(auth);
    }

    onKeyPress(event) {
        if (event.key !== 'Enter') {
            return;
        }

        event.stopPropagation();

        // If the user hits enter, it's logically the
        // same as clicking 'Next.'
        const auth = {
            identity: this.state.identity,
            password: this.state.password
        };

        this.props.submitAuthData(auth);
    }

    render() {
        let component = (
            <div className={'network-authentication-dialog'}>
                <InputField
                    onChange={this.onChangeUserNameField}
                    onKeyPress={this.onKeyPress}
                    value={this.state.identity}
                    placeholder={'User name'}
                    label={'Enter your user name and password'}
                />
                <InputField
                    onChange={this.onChangePasswordField}
                    onKeyPress={this.onKeyPress}
                    value={this.state.password}
                    placeholder={'Password'}
                    type={'password'}
                />
                <div className={'dialog-nav-row'}>
                    <Button
                        onClick={this.onClickNext}
                        className={'dialog-nav-button'}>
                        Next
                    </Button>
                    <Button
                        onClick={this.props.onClickCancel}
                        className={'dialog-nav-button'}>
                        Cancel
                    </Button>
                </div>
            </div>
        );

        return component;
    }
}
