import React, {Component} from 'react';
import {ipcRenderer} from 'electron';

import NetworkListItem from './network_list_item';
import Button from './button';

// A SavedNetworkOptionsDialog is a dialog shown when the user
// wants to manage a saved network.
export default class SavedNetworkOptionsDialog extends Component {
    constructor(props) {
        super(props);

        this.onClickForget = this.onClickForget.bind(this);
    }

    onClickForget() {
        ipcRenderer.send('forget-network', {ssid: this.props.ssid})
    }

    render() {
        let component = (
             <div className={'saved-network-options-dialog'} onClick={this.props.itemClicked}>
                <NetworkListItem
                    ssid={this.props.ssid}
                    security={this.props.security}
                    signal={'excellent'}
                    itemClicked={this.props.itemClicked}
                />
                <Button
                    className={'saved-network-forget-button'}
                    onClick={this.onClickForget}>
                    Forget
                </Button>
            </div>
        );

        return component;
    }
}
