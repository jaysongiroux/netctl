import React, {Component} from 'react';

import Button from './button';
import NetworkListItem from './network_list_item';

import {ipcRenderer} from 'electron';

export default class ConnectedNetworkDialog extends Component {
    constructor(props) {
        super(props);

        this.onClickDisconnect = this.onClickDisconnect.bind(this);
    }

    onClickDisconnect() {
        ipcRenderer.send('disconnect', null);
    }

    render() {
        let component = (
            <div className={'selected-network-dialog'} onClick={this.props.itemClicked}>
                <NetworkListItem
                    ssid={this.props.ssid}
                    signal={this.props.signal}
                    security={this.props.security}
                    isConnected={true}
                    itemClicked={this.props.itemClicked}
                />
                <Button
                    className={'selected-network-dialog-button'}
                    onClick={this.onClickDisconnect}>
                    Disconnect
                </Button>
            </div>
        );

        return component;
    }
}
