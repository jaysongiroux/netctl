import React, {Component} from 'react';

import ReactLoading from 'react-loading';

import Button from './button';
import Checkbox from './checkbox';
import NetworkListItem from './network_list_item';

import AuthenticationDialogPSK from './authentication_dialog_psk';
import AuthenticationDialogEAP from './authentication_dialog_eap';
import AuthenticationDialogOpen from './authentication_dialog_open';
import HiddenNetworkDialog from './hidden_network_dialog';

import {ipcRenderer} from 'electron';

// The stages of a NetworkConnectionDialog.
const stages = {
    // Indicates that the default NetworkConnectionDialog
    // should be rendered.
    DEFAULT: 0,
    // Indicates that the authentication dialog, if any,
    // should be rendered. Triggered by clicking on
    // the 'Connect' button.
    AUTH_DIALOG: 1,
    // Indicates that a connection attempt is currently
    // in progress, and the dialog is waiting for a result.
    CONNECTING: 2,
    // Indicates that the network being connected to is hidden,
    // and a prompt for the SSID name should be rendered.
    HIDDEN_SSID: 3
}

export default class NetworkConnectionDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: stages.DEFAULT,
            cfg: {
                ssid: this.props.ssid,
                security: this.props.security,
                // If this is a hidden network, the scan_ssid option
                // needs to be set in the network configuration.
                scanSSID: !this.props.ssid,

                // This is controlled by a checkbox. It indicates whether
                // netctl is allowed to automatically connect to this network
                // when it is in range.
                noAutoConnect: false,
            }
        };

        this.onClickConnect = this.onClickConnect.bind(this);
        this.onAuthDialogCancel = this.onAuthDialogCancel.bind(this);
        this.onAutoConnectChange = this.onAutoConnectChange.bind(this);
        this.doConnectionAttempt = this.doConnectionAttempt.bind(this);
        this.setHiddenNetworkSSID = this.setHiddenNetworkSSID.bind(this);
        this.tryConnectNoPrompt = this.tryConnectNoPrompt.bind(this);

        // Register IPC event handlers.
        this.handleConnectionFailed = this.handleConnectionFailed.bind(this);
        this.handleNoSavedConfiguration = this.handleNoSavedConfiguration.bind(this);

        ipcRenderer.on('connection-failed', this.handleConnectionFailed);
        ipcRenderer.on('no-saved-configuration', this.handleNoSavedConfiguration);
    }

    onClickConnect(event) {
        // We do not want this click event to propagate. It
        // will be handled within this component.
        event.stopPropagation();

        // If no SSID is set, prompt the user for SSID name.
        if ( !(this.props.ssid) ) {
            this.setState((state) => ({...state, stage: stages.HIDDEN_SSID}));
            return;
        }

        // If we already have the SSID, we can look for a
        // saved configuration. If the branch above was
        // taken, it will call this same method once an
        // SSID has been given.
        this.tryConnectNoPrompt();
    }

    tryConnectNoPrompt() {
        this.setState((state) => ({...state, stage: stages.CONNECTING}));

        const arg = {
            cfg: this.state.cfg,
            trySavedConfiguration: true,
        };

        // If this fails because there is no saved configuration,
        // or because the saved config is out of date, an auth
        // dialog will be shown in response.
        ipcRenderer.send('connect', arg);
    }

    // This function will be passed to auth dialogs to use as a callback.
    doConnectionAttempt(auth) {
        this.setState((state) => ({...state, stage: stages.CONNECTING}));

        // Send the configuration to the main process. The result
        // of the connection attempt will be sent back via the
        // 'connection-result' event.
        const arg = {
            cfg: {
                ...this.state.cfg,
                ...auth,
            },
            trySavedConfiguration: false,
        };

        ipcRenderer.send('connect', arg);
    }

    setHiddenNetworkSSID(ssid) {
        // Now that we got the SSID name, we can carry on
        // with the connection dialog.
        this.setState((state) => ({
            ...state,
            cfg: {
                ...state.cfg,
                ssid: ssid
            }
        }));

        this.tryConnectNoPrompt();
    }

    onAuthDialogCancel(event) {
        this.setState((state) => ({...state, stage: stages.DEFAULT}));
    }

    onAutoConnectChange(event) {
        const value = !event.target.checked;

        this.setState((state) => ({
            ...state,
            cfg: {
                ...state.cfg,
                // The checkbox is asking if we _should_
                // enable auto connect. But the configuration
                // has no_auto_connect (to preserve old behavior/
                // zero values). Negate the value we are given.
                noAutoConnect: value,
            },
        }));
    }

    handleConnectionFailed(event) {
        this.setState((state) => ({...state, stage: stages.AUTH_DIALOG}));
    }

    handleNoSavedConfiguration(event) {
        this.setState((state) => ({...state, stage: stages.AUTH_DIALOG}));
    }

    getAuthDialog() {
        let dialog;

        switch(this.props.security) {
            case 'wpa-psk':
                dialog = (
                    <AuthenticationDialogPSK
                        submitAuthData={this.doConnectionAttempt}
                        onClickCancel={this.onAuthDialogCancel}
                    />
                );
                break;

            case 'wpa-eap':
                dialog = (
                    <AuthenticationDialogEAP
                        submitAuthData={this.doConnectionAttempt}
                        onClickCancel={this.onAuthDialogCancel}
                    />
                );
                break;

            case 'unknown':
                // wpa_supplicant gives an unknown auth state
                // for open networks.
                dialog = (
                    <AuthenticationDialogOpen
                        doConnectionAttempt={this.doConnectionAttempt}
                    />
                );
                break
        }

        return dialog;
    }

    render() {
        let component;

        switch(this.state.stage) {
            case stages.DEFAULT:
                component = (
                    <div className={'selected-network-dialog'} onClick={this.props.itemClicked}>
                        <NetworkListItem
                            ssid={this.props.ssid}
                            signal={this.props.signal}
                            security={this.props.security}
                            itemClicked={this.props.itemClicked}
                        />
                        <Checkbox
                                label={'Connect automatically'}
                                checked={!this.state.cfg.noAutoConnect}
                                onChange={this.onAutoConnectChange}
                        />
                        <Button
                            className={'selected-network-dialog-button'}
                            onClick={this.onClickConnect}>
                            Connect
                        </Button>
                    </div>
                );
                break;

            case stages.AUTH_DIALOG:
                // 'Connect' button was clicked. Show the auth dialog.
                const authDialog = this.getAuthDialog();

                component = (
                    <div className={'selected-network-dialog'} onClick={this.props.itemClicked}>
                        <NetworkListItem
                            ssid={this.props.ssid}
                            signal={this.props.signal}
                            security={this.props.security}
                            itemClicked={this.props.itemClicked}
                        />
                        {authDialog}
                    </div>
                );
                break;

            case stages.HIDDEN_SSID:
                component = (
                    <div className={'selected-network-dialog'} onClick={this.props.itemClicked}>
                        <NetworkListItem
                            ssid={this.props.ssid}
                            signal={this.props.signal}
                            security={this.props.security}
                            itemClicked={this.props.itemClicked}
                        />
                        <HiddenNetworkDialog
                            submitSSID={this.setHiddenNetworkSSID}
                        />
                    </div>
                );
                break;

            case stages.CONNECTING:
                component = (
                    <div className={'selected-network-dialog'} onClick={this.props.itemClicked}>
                        <NetworkListItem
                            ssid={this.props.ssid}
                            signal={this.props.signal}
                            security={this.props.security}
                            itemClicked={this.props.itemClicked}
                        />
                        <div className={'network-auth-spinner'}>
                            <ReactLoading
                                type={'spinningBubbles'}
                                color={'white'}
                                height={'15%'}
                                width={'15%'}
                            />
                        </div>
                    </div>
                );
        }

        return component;
    }

}
