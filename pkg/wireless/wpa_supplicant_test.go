// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"reflect"
	"sort"
	"testing"
)

func TestScanResultsSort(t *testing.T) {
	sorted := scanResults{
		{SignalStrength: -20},
		{SignalStrength: -40},
		{SignalStrength: -68},
		{SignalStrength: -80},
		{SignalStrength: 1},
	}

	unsorted := scanResults{
		{SignalStrength: -68},
		{SignalStrength: -20},
		{SignalStrength: 1},
		{SignalStrength: -80},
		{SignalStrength: -40},
	}

	sort.Sort(unsorted)

	if !reflect.DeepEqual(sorted, unsorted) {
		t.Fatalf("scan results were not sorted as expected:\ngot: %+v\nwanted: %+v", unsorted, sorted)
	}
}
